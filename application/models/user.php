<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP User Model
 *
 * Este modelo es el encargado de interactuar con la tabla user
 * de la base de datos kp_db
 *
 * @package     Model
 * @category    Model
 * @author      Javier Mota Diaz
 */
class User extends CI_Model{
    // Campos de user en la base de datos, con visibilidad restringida
    // para accder a ellas desde fuera, se necesitaran getters y setters

    /**
     * Id del modelo. generado por autoincrement.
     *
     * @var integer
     */
    protected $id = NULL;

    /**
     * Nombre del usuario
     *
     * @var string
     */
    protected $name = NULL;

    /**
     * Email del usuario. es campo unique
     *
     * @var string
     */
    protected $email = NULL;

    /**
     * Password del usuario. Se guardara en la db con una encriptacion sha1
     *
     * @var string
     */
    protected $password = NULL;

    /**
     * Role del usuario. Permisos para determinadas acciones y accesos
     *
     * @var string
     */
    protected $role = NULL;

    /**
     * Constructor
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Get id;
     *
     * @return integer/NULL id del usuario
     */
    public function get_id(){
        return $this->id;
    }

    /**
     * Get name;
     *
     * @return integer/NULL name del usuario
     */
    public function get_name(){
        return $this->name;
    }

    /**
     * Set name. Prepara a la db para un insert o un update
     *
     * @param string $name valor a establecer en el campo name
     */
    public function set_name($name){
        $this->name = $name;
        $this->db->set('name', $this->name);
    }


    /**
     * Get email;
     *
     * @return integer/NULL email del usuario
     */
    public function get_email(){
        return $this->email;
    }

    /**
     * Set email. Prepara a la db para un insert o un update
     *
     * @param string $email valor a establecer en el campo email
     */
    public function set_email($email){

        $sql = "SELECT * FROM user WHERE email = ?";
        $query = $this->db->query($sql, array($email));

        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->email = $email;
            $this->db->set('email', $this->email);

            return TRUE;
        }
    }

    /**
     * Get password;
     *
     * @return integer/NULL password del usuario
     */
    public function get_password(){
        return $this->password;
    }

    /**
     * Set password. Prepara a la db para un insert o un update
     *
     * @param string $password valor a establecer en el campo password
     */
    public function set_password($password){
        $this->password = sha1($password);
        $this->db->set('password', $this->password);
    }

    /**
     * Get role;
     *
     * @return integer/NULL role del usuario
     */
    public function get_role(){
        return $this->role;
    }

    /**
     * Set role. Prepara a la db para un insert o un update
     *
     * @param string $role valor a establecer en el campo role
     */
    public function set_role($role){
        $this->role = $role;
        $this->db->set('role', $this->role);
    }

    /**
     * Realiza un insert con todos los set realizados
     */
    public function insert()
    {
        $this->db->insert('user');
    }

    /**
     * Realiza un insert con todos los set realizados
     */
    public function update()
    {
        $this->db->where('id', $this->get_id());
        $this->db->update('user');
    }

    /**
     * Carga un usuario en el modelo a traves de un id dado
     * Retornara TRUE o FALSE si hay algun usuario con ese id
     *
     * @param  integer $id id de usuario
     * @return boolean comunica si ha sido posible la carga
     */
    public function load_by_id($id)
    {
        $sql = "SELECT * FROM user WHERE id = ?";
        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $this->_load_user($query->row());

            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Carga un usuario en el modelo a traves de email y password (login)
     * Retornara TRUE o FALSE si hay algun usuario con ese login
     *
     * @param  string $email email introducido en el login
     * @param  string $password password introducido en el login
     * @return boolean comunica si ha sido posible la carga
     */
    public function load_by_login($email, $password)
    {
        $password = sha1($password);
        $sql = "SELECT * FROM user
                WHERE email = ? AND password = ?";
        $query = $this->db->query($sql, array($email, $password));

        if ($query->num_rows() > 0) {
            $this->_load_user($query->row());

            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Obtinene todos los users de la tabla, o false si no hay ninguno
     *
     * @return Object/FALSE Objeto con todos los users de la tabla
     */
    public function get_all_users()
    {
        $query = $this->db->query('SELECT * FROM user');

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return FALSE;
    }

    /**
     * Crea un usuario tomando todos los campos
     *
     * @param  string $name nombre del usuario
     * @param  string $email email del usuario
     * @param  string $password password del usuario
     * @param  string $role permisos del usuario
     */
    public function create_user($name, $email, $password, $role)
    {
        $this->set_name($name);
        $this->set_email($email);
        $this->set_password($password);
        $this->set_role($role);
        $this->insert();
    }

    /**
     * Carga un usuario en el modelo
     *
     * @param  Object $row resultado de una consulta. Una fila
     */
    private function _load_user($row)
    {
        foreach ($row as $key => $value) {
            $this->{$key} = $value;
        }
    }

}
/* End of file user.php */
/* Location: application/post/user.php */