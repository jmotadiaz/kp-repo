<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP Post Model
 *
 * Este modelo es el encargado de interactuar con la tabla post
 * de la base de datos kp_db
 *
 * @package     Model
 * @category    Model
 * @author      Javier Mota Diaz
 */
class Post extends CI_Model{
    // Campos de post en la base de datos, con visibilidad restringida
    // para accder a ellas desde fuera, se necesitaran getters y setters

    /**
     * Id del modelo. generado por autoincrement.
     *
     * @var integer
     */
    protected $id = NULL;

    /**
     * Titulo del post
     *
     * @var string
     */
    protected $title = NULL;

    /**
     * Fecha del post
     *
     * @var \Date
     */
    protected $date = NULL;

    /**
     * Contenido del post
     *
     * @var string
     */
    protected $content = NULL;

    /**
     * Status del post. ACTIVE o CENSURED
     *
     * @var string
     */
    protected $status = NULL;

    /**
     * Id del user dueño del post
     *
     * @var integer
     */
    protected $user_id = NULL;

    /**
     * Constructor
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer/NULL id del post
     */
    public function get_id(){
        return $this->id;
    }

    /**
     * Get title
     *
     * @return integer/NULL title del post
     */
    public function get_title(){
        return $this->title;
    }

    /**
     * Set title. Prepara a la db para un insert o un update
     *
     * @param string $title valor a establecer en el campo title
     */
    public function set_title($title){
        $this->title = $title;
        $this->db->set('title', $this->title);
    }


    /**
     * Get date
     *
     * @return integer/NULL date del post
     */
    public function get_date(){
        return $this->date;
    }

    /**
     * Set date. Prepara a la db para un insert o un update
     *
     * @param string $date valor a establecer en el campo date
     */
    public function set_date($date){
        $this->date = new DateTime($date);
        $this->db->set('date', $this->date->format('Y-m-d H:i:s'));
    }

    /**
     * Get content
     *
     * @return integer/NULL content del post
     */
    public function get_content(){
        return $this->content;
    }

    /**
     * Set content. Prepara a la db para un insert o un update
     *
     * @param string $content valor a establecer en el campo content
     */
    public function set_content($content){
        $this->content = $content;
        $this->db->set('content', $this->content);
    }

    /**
     * Get status. Prepara a la db para un insert o un update
     *
     * @return integer/NULL status del post
     */
    public function get_status(){
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status valor a establecer en el campo status
     */
    public function set_status($status){
        $this->status = $status;
        $this->db->set('status', $this->status);
    }

    /**
     * Get user_id
     *
     * @return integer/NULL user_id del post
     */
    public function get_user_id(){
        return $this->user_id;
    }

    /**
     * Set user_id
     *
     * @param string $user_id valor a establecer en el campo user_id
     */
    public function set_user_id($user_id){
        $this->user_id = $user_id;
        $this->db->set('user_id', $this->user_id);
    }

    /**
     * Realiza un insert con todos los set realizados
     */
    public function insert()
    {
        $this->db->insert('post');
    }

    /**
     * Realiza un insert con todos los set realizados
     */
    public function update()
    {
        $this->db->where('id', $this->get_id());
        $this->db->update('post');
    }

    /**
     * Crea un post tomando todos los campos
     *
     * @param  string $title nombre del usuario
     * @param  string $date date del usuario
     * @param  string $content content del usuario
     * @param  string $status permisos del usuario
     * @param  string $user_id permisos del usuario
     */
    public function create_post($title, $date, $content, $status, $user_id)
    {
        $this->set_title($title);
        $this->set_date($date);
        $this->set_content($content);
        $this->set_status($status);
        $this->set_user_id($user_id);
        $this->insert();
    }

    /**
     * Carga un post en el modelo a traves de un id dado
     * Retornara TRUE o FALSE si hay algun post con ese id
     *
     * @param  integer $id id de post
     * @return boolean comunica si ha sido posible la carga
     */
    public function load_by_id($id)
    {
        $sql = "SELECT * FROM post WHERE id = ?";
        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $this->_load_post($query->row());

            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Retorna los posts pertenecientes a un user dado
     * Retornara FALSE si no hay ningun post de ese usuario
     *
     * @param  integer $user_id email introducido en el login
     * @return Object/False objeto con los posts o false si no obtiene resultado
     */
    public function get_by_user($user_id)
    {
        $sql = "SELECT * FROM post WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Obtinene todos los posts de la tabla en formato array
     *
     * @return Array array con todos los posts de la tabla
     */
    public function get_array_posts()
    {
        $query = $this->db->query('SELECT * FROM post');

        return $query->result_array();
    }

    /**
     * Obtiene los posts activos por paginacion
     *
     * @param  integer $num numero de post
     * @param  integer $offset a partir de que post empieza a obtener
     * @return Object/FALSE los posts seleccionados o false
     */
    public function get_by_pagination($num, $offset)
    {
        $this->db->order_by('date', 'desc');
        $query = $this->db->get_where('post',
         array('status' => 'ACTIVE'), $num, $offset);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return FALSE;
    }

    /**
     * Obtinene el numero de posts activos que hay en la tabla
     *
     * @return integer numero de posts en la tabla
     */
    public function get_active_count()
    {
        $query = $this->db->query('SELECT * FROM post WHERE status = "ACTIVE"');

        return $query->num_rows();
    }

    /**
     * Carga un post en el modelo
     *
     * @param  Object $row resultado de una consulta. Una fila
     */
    private function _load_post($row)
    {
        foreach ($row as $key => $value) {
            $this->{$key} = $value;
        }

        $this->date = new \Datetime($this->date);
    }
}
/* End of file post.php */
/* Location: application/model/post.php */