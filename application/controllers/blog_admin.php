<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP Blog_admin Controller
 *
 * Este controlador se encarga de gestionar la seccion
 * del administrador del blog
 *
 * @package     Controller
 * @category    Controller
 * @author      Javier Mota Diaz
 */
class Blog_admin extends CI_Controller {

    /**
     * Gestiona la pagina del panel del administrador
     *
     * @param  integer $id id del primer post para mostrar en la paginacion
     * @return view vista de posts con paginacion
     */
    public function panel()
    {
        $this->load->view('blog/admin_section/panel');
    }

    /**
     * Devuelve un json de posts
     */
    public function posts_json()
    {
        $this->load->model('Post');
        header('Content-Type: application/json');
        echo json_encode(array('aaData' =>$this->Post->get_array_posts()));
    }

    /**
     * Censura un post
     *
     * @param  array  $params los parametros en los segmentos de la uri
     */
    public function post_censure($params)
    {
        $this->load->model('Post');

        if ($this->Post->load_by_id($params[0])) {
            $this->Post->set_status('CENSURED');
            $this->Post->update();
        }
        redirect(URL.'blog_admin/panel');
    }

    /**
     * Reactiva un post
     *
     * @param  array  $params los parametros en los segmentos de la uri
     */
    public function post_uncensure($params)
    {
        $this->load->model('Post');

        if ($this->Post->load_by_id($params[0])) {
            $this->Post->set_status('ACTIVE');
            $this->Post->update();
        }
        redirect(URL.'blog_admin/panel');
    }

    /**
     * Comprueba que el usuario es admin para todas las rutas
     * del controlador
     *
     * @param  string $method el action solicitado
     * @param  array  $params los parametros en los segmentos de la uri
     */
    public function _remap($method, $params = array())
    {
        if ($this->_is_authorized_admin())
        {
            $this->$method($params);
        } else {

            if ($method === 'posts_json') {
                echo null;
            } else {
                $this->load->view('blog/public_section/no_authorized');
            }
        }
    }

    /**
     * Comprueba si el usuario esta logueado
     * y tiene rol USER o ADMIN
     *
     * @return boolean resultado de la comprobacion
     */
    private function _is_authorized_admin()
    {
        if ($this->session->userdata('is_logging')) {
            $this->load->model('User');
            $this->User->load_by_id($this->session->userdata('user_id'));
            $user_role = $this->User->get_role();

            if ($user_role === 'ADMIN') {
                return TRUE;
            }
        }

        return FALSE;
    }

}

/* End of file index.php */
/* Location: ./application/controllers/index.php */