<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP Blog_public Controller
 *
 * Este controlador se encarga de gestionar las rutas relacionadas
 * con la seccion publica del blog
 *
 * @package     Controller
 * @category    Controller
 * @author      Javier Mota Diaz
 */
class Blog_public extends CI_Controller {

    /**
     * Gestiona la seccion de posts del blog
     * Mostrando una serie de posts con paginacion
     *
     * @param  integer $id id del primer post para mostrar en la paginacion
     * @return view vista de posts con paginacion
     */
    public function posts($id = 0)
    {
        $this->load->library('pagination');
        $this->load->model('Post');

        $config['base_url'] = URL.'blog_public/posts';
        $config['total_rows'] = $this->Post->get_active_count();
        $config['per_page'] = 3;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['posts'] = $this->Post->
        get_by_pagination($config['per_page'], $id);

        if ($data['posts']) {
            $this->load->view('blog/public_section/posts', $data);
        } else {
            $this->load->view('blog/public_section/no_result');
        }
    }

    /**
     * Gestiona la seccion de creacion de post
     * comprobando si tiene permiso, y enviando un form para la creacion del post
     *
     * @return view vista con formulario de creacion de post
     */
    function create_post()
    {
        if ($this->_is_authorized_user()) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');

            if ($this->form_validation->run('post') == FALSE)
            {
                $this->load->view('blog/public_section/create_post');
            }
            else
            {
                $this->load->model('Post');
                $this->Post->create_post(
                    $this->input->post('title'),
                    "now",
                    $this->input->post('content'),
                    'ACTIVE',
                    $this->session->userdata('user_id')
                );

                redirect(URL.'blog_public/posts');
            }
        } else {
            $this->load->view('blog/public_section/no_authorized');
        }
    }

    /**
     * Comprueba si el usuario esta logueado
     * y tiene rol USER o ADMIN
     *
     * @return boolean resultado de la comprobacion
     */
    private function _is_authorized_user()
    {
        if ($this->session->userdata('is_logging')) {
            $this->load->model('User');
            $this->User->load_by_id($this->session->userdata('user_id'));
            $user_role = $this->User->get_role();

            if ($user_role === 'USER' || $user_role === 'ADMIN') {
                return TRUE;
            }
        }

        return FALSE;
    }

}

/* End of file index.php */
/* Location: ./application/controllers/index.php */