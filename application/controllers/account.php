<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP Account Controller
 *
 * Este controlador se encarga de gestionar las rutas relacionadas
 * con el logueo o creacion de usuarios
 *
 * @package     Controller
 * @category    Controller
 * @author      Javier Mota Diaz
 */
class Account extends CI_Controller {

    /**
     * Action que gestiona la url /blog/account/login
     *
     * Manda un login form si el formulario no es valido o el user no coincide
     * o inicia sesion si el user es encontrado
     *
     * @return view Formulario login
     */
    public function login()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->form_validation->run('login') == FALSE)
        {
            $this->load->view('blog/account/login');
        }
        else
        {
            $this->load->model('User');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            if ($this->User->load_by_login($email, $password)) {
                $this->_set_session($this->User);
                redirect('blog_public/posts', 'refresh');
            } else {
                $data['login_fail_message'] = 'Invalid username/email or password';
                $this->load->view('blog/account/login', $data);
            }
        }
    }

    /**
     * Action que gestiona la url /blog/account/signup
     *
     * Manda un signup form si el formulario no es valido
     * o crea un user e inicia su sesion
     *
     * @return view Formulario signup
     */
    public function signup()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if ($this->form_validation->run('signup') == FALSE)
        {
            $this->load->view('blog/account/signup');
        }
        else
        {
            $this->load->model('User');
            $this->User->create_user(
                $this->input->post('name'),
                $this->input->post('email'),
                $this->input->post('password'),
                'USER'
            );
            $this->User->load_by_login(
                $this->input->post('email'),
                $this->input->post('password')
            );
            $this->_set_session($this->User);
            redirect(URL.'blog_public/posts', 'refresh');
        }
    }

    /**
     * Elimina la sesion del usuario
     */
    public function logout()
    {
        $this->_delete_session();
        redirect('blog_public/posts');
    }

    /**
     * Establece los datos del usuario en la sesion
     *
     * @param User $user usuario que inicia session
     */
    private function _set_session($user)
    {
        $params = array('api_key' => WURLF_API_KEY);
        $this->load->library('mobile_agent', $params);
        $this->session->set_userdata('is_logging', TRUE);
        $this->session->set_userdata('user_name', $user->get_name());
        $this->session->set_userdata('user_id', $user->get_id());
        $mobile = $this->mobile_agent->is_smartphone();

        if($this->mobile_agent->is_smartphone()) {
            $this->session->set_userdata('is_smartphone', TRUE);
            $this->session->set_userdata('brand_name', $this->mobile_agent->brand_name());
            $this->session->set_userdata('model_name', $this->mobile_agent->model_name());
        } else {
            $this->session->set_userdata('is_smartphone', FALSE);
        }
    }

    /**
     * Elimina los datos del usuario en la sesion
     */
    private function _delete_session()
    {
        $this->session->unset_userdata('is_logging');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('is_smartphone');
        $this->session->unset_userdata('brand_name');
        $this->session->unset_userdata('model_name');
    }

}

/* End of file index.php */
/* Location: ./application/controllers/blog/account.php */