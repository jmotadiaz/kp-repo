<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'third_party/WurflCloudClient/src/autoload.php';
/**
 * KP
 *
 * Simple blog prueba Kitmaker Entertainment
 *
 * @author      Javier Mota Diaz
 */

// ------------------------------------------------------------------------

/**
 * KP Mobile_agent
 *
 * Proporciona una interfaz para interactuar con Wurlf Cloud Client
 *
 * @package     Library
 * @category    Library
 * @author      Javier Mota Diaz
 */
class Mobile_agent {

    /**
     * WurlfCloud config
     *
     * @var ScientiaMobile\WurflCloud\Config
     */
    protected $config;

    /**
     * WurlfCloud config
     *
     * @var ScientiaMobile\WurflCloud\Client
     */
    protected $client;

    /**
     * Constructor
     *
     * Inicia WurlfCloud\Config con la api_key recibida
     * e instancia un WurlfCloud\Client con dicha configuracion
     *
     * @param array $params contiene la api_key para instanciar un WurlfCloud\Client
     */
    public function __construct($params) {
        // Create a configuration object
        $this->config = new ScientiaMobile\WurflCloud\Config();

        // Set your WURFL Cloud API Key
        $this->config->api_key = $params['api_key'];

        // Create the WURFL Cloud Client
        $this->client = new ScientiaMobile\WurflCloud\Client($this->config);
        $this->client->detectDevice();
    }

    /**
     * Detecta si el user agent es un movil
     *
     * @return boolean
     */
    public function is_smartphone()
    {
        return $this->client->getDeviceCapability('is_smartphone');
    }

    /**
     * Detecta la marca del movil
     *
     * @return string marca del movil
     */
    public function brand_name()
    {
        return $this->client->getDeviceCapability('brand_name');
    }

    /**
     * Detecta el modelo del movil
     *
     * @return string modelo del movil
     */
    public function model_name()
    {
        return $this->client->getDeviceCapability('model_name');
    }
}

/* End of file Mobile_agent.php */
/* Location: ./application/libraries/Mobile_agent.php */