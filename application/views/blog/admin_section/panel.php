<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blog posts</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'bootstrap/dist/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo (CSS.'blog-admin.css'); ?>">
</head>
<body>
  <?php $this->load->view('blog/blog_header') ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <table id="admin-panel" class="table admin-panel"></table>
      </div>
    </div>
  </div>

  <script src="<?php echo (COMPONENTS.'jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo (COMPONENTS.'datatables/media/js/jquery.dataTables.js'); ?>"></script>
  <script src="<?php echo (COMPONENTS.'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js'); ?>"></script>
  <script>
  $(document).ready(function() {
    $('#admin-panel').dataTable( {
      "bProcessing": true,
      "sAjaxSource": "<?php echo URL.'blog_admin/posts_json'; ?>",
      "aoColumns": [
            { "sTitle": "Id", "mData": "id" },
            { "sTitle": "Title", "mData": "title", "sClass": "col-content" },
            { "sTitle": "Date", "mData": "date" },
            { "sTitle": "Content", "mData": "content", "sClass": "col-content" },
            { "sTitle": "Status", "mData": "status" },
            { "sTitle": "User id", "mData": "user_id" },
            {
               "mData": null,
               "sTitle": "Action",
               "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var tdAction;
                  var link;
                  var url = "<?php echo URL.'blog_admin/post_'; ?>"

                  if (oData.status === 'ACTIVE') {
                    tdAction = 'censure';
                    $(nTd).closest('tr').removeClass('row-censured');
                  } else {
                    tdAction = 'uncensure';
                    $(nTd).closest('tr').addClass('row-censured');
                  }
                  link = '<a href="' + url + tdAction +
                  '/' + oData.id + '">' + tdAction +'</a>';
                  $(nTd).html(link);
               }
            }
      ]
    });
    $('#admin-panel_filter').find('input').addClass('form-control');
  });
  </script>
</body>
</html>