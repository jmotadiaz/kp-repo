<?php if ($this->session->userdata('is_logging')): ?>
  <div class="alert alert-info">
    <p>Wellcome <?php echo $this->session->userdata('user_name') ?></p>
    <?php echo anchor(URL.'account/logout', 'logout'); ?>
  </div>
  <?php if ($this->session->userdata('is_smartphone')): ?>
  <div class="alert alert-info">
    <p>
      Your mobile:
      <?php echo $this->session->userdata('brand_name') ?>
      &nbsp;
      <?php echo $this->session->userdata('model_name') ?>
    </p>
  </div>
  <?php endif ?>
<?php else: ?>
  <div class="alert alert-warning">
    <p>No ha iniciado sesion</p>
    <p>
      Si quiere aqui puede hacer
      <?php echo anchor(URL.'account/login', 'login'); ?>
    </p>
  </div>
<?php endif ?>