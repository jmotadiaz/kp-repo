<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blog login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'bootstrap/dist/css/bootstrap.min.css'); ?>">
</head>
<body>
  <div class="login-container" style="max-width: 400px; margin: 50px auto 0 auto; padding: 0 15px 15px 15px;">
    <h1 class="text-center">Login</h1>
    <?php if (isset($login_fail_message)): ?>
    <p class="text-danger"><?php echo $login_fail_message ?></p>
    <?php endif ?>

    <?php
    $attributes = array('class' => 'form', 'novalidate' => '');
    echo form_open('account/login', $attributes);
    ?>

    <?php $email_error = form_error('email'); ?>

    <div class="form-group <?php if ($email_error) {echo "has-error";} ?>">
      <label class="control-label">Email Address</label>
      <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>"/>
      <span class="help-inline"><?php echo $email_error ?></span>
    </div>

    <?php $password_error = form_error('password'); ?>

    <div class="form-group <?php if ($password_error) {echo "has-error";} ?>">
      <label class="control-label">Password</label>
      <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>"/>
      <span class="help-inline"><?php echo $password_error ?></span>
    </div>

    <div>
      <input class="btn btn-success" type="submit" value="Submit" />
      <span>
        &nbsp;
        Or
        &nbsp;
      </span>
      <?php echo anchor(URL.'account/signup', 'Signup'); ?>
    </div>

    </form>
  </div>

  <script src="<?php echo (COMPONENTS.'jquery/jquery.min.js'); ?>"></script>
</body>
</html>