<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blog posts</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'bootstrap/dist/css/bootstrap.min.css'); ?>">
</head>
<body>
  <?php $this->load->view('blog/blog_header') ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <h3>Add a Post</h3>

        <?php
        $attributes = array('class' => 'form', 'novalidate' => '');
        echo form_open(current_url(), $attributes);
        ?>

        <?php $title_error = form_error('title'); ?>

        <div class="form-group <?php if ($title_error) {echo "has-error";} ?>">
          <label class="control-label">Post Title</label>
          <input class="form-control" type="text" name="title" value="<?php echo set_value('title'); ?>"/>
          <span class="help-inline"><?php echo $title_error ?></span>
        </div>

        <?php $content_error = form_error('content'); ?>

        <div class="form-group <?php if ($content_error) {echo "has-error";} ?>">
          <label class="control-label">Post Content</label>
          <textarea class="form-control" name="content" rows="10"><?php echo set_value('content'); ?></textarea>
          <span class="help-inline"><?php echo $content_error ?></span>
        </div>

        <div>
          <input class="btn btn-success" type="submit" value="Submit" />
        </div>

      </div>
      <div class="col-sm-4 col-sm-offset-1" style="margin-top: 20px;">
        <?php $this->load->view('blog/blog_sidebar') ?>
      </div>
    </div>
  </div>

  <script src="<?php echo (COMPONENTS.'jquery/jquery.min.js'); ?>"></script>
</body>
</html>