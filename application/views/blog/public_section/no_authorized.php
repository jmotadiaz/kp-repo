<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blog posts</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'bootstrap/dist/css/bootstrap.min.css'); ?>">
</head>
<body>
  <?php $this->load->view('blog/blog_header') ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <h3>User no authorized</h3>
        <div>
          <?php echo anchor(URL.'account/login', 'Login'); ?>
          &nbsp; Or &nbsp;
          <?php echo anchor(URL.'blog_public/posts', 'Index'); ?>
        </div>
      </div>
      <div class="col-sm-4 col-sm-offset-1" style="margin-top: 20px;">
        <?php $this->load->view('blog/blog_sidebar') ?>
      </div>
    </div>
  </div>

  <script src="<?php echo (COMPONENTS.'jquery/jquery.min.js'); ?>"></script>
</body>
</html>