<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Blog posts</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo (COMPONENTS.'bootstrap/dist/css/bootstrap.min.css'); ?>">
</head>
<body>

  <?php $this->load->view('blog/blog_header') ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-7">

        <?php foreach ($posts as $post): ?>
          <?php $this->Post->load_by_id($post->id); ?>
          <h3>
            <?php echo $this->Post->get_title() ?>
            <small class="text-muted">&nbsp; <?php echo $this->Post->get_date()->format('Y/m/d H:i') ?></small>
          </h3>
          <p><?php echo $this->Post->get_content() ?></p>
          <hr>
        <?php endforeach ?>

        <?php echo $pagination ?>
      </div>
      <div class="col-sm-4 col-sm-offset-1" style="margin-top: 20px;">
        <?php $this->load->view('blog/blog_sidebar') ?>
        <a href="<?php echo URL.'blog_public/create_post'; ?>" class="btn btn-primary btn-block">
          <span class="glyphicon glyphicon-plus"></span>
          Create Post
        </a>
      </div>
    </div>
  </div>

  <script src="<?php echo (COMPONENTS.'jquery/jquery.min.js'); ?>"></script>
</body>
</html>