<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Form validations
|--------------------------------------------------------------------------
| Establece una serie de condiciones para distintos formulario, ej: signup
*/
$config = array(
    // Para validaciones do formularios signup
    'signup' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|max_lenght[45]'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email|is_unique[user.email]'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    ),
    // Para validaciones do formularios login
    'login' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    ),
    'post' => array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'required|max_lenght[140]'
        ),
        array(
            'field' => 'content',
            'label' => 'Content',
            'rules' => 'required'
        )
    )
);